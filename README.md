این مخزن صرفن برای مستندات پروژه به‌کار می‌رود.

[درباره‌ی ما](about_us.md)  
[شیوه‌نامه](CONTRIBUTING.md)  
[مرام‌نامه](code-of-conduct.md)  
[قوانین](Rules.md)  
[مجوز‌ها](licenses.md)  
[TODO List](TODO_List.md)